<?php

use Illuminate\Database\Seeder;

class PlaylistsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('playlists')->delete();
        
        \DB::table('playlists')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Playlist seeded by Gordi',
                'artist_id' => 1,
                'created_at' => '2020-06-07 12:16:38',
                'updated_at' => '2020-06-07 12:16:38',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Playlist seeded by 10,000 Maniacs',
                'artist_id' => 50,
                'created_at' => '2020-06-07 12:39:37',
                'updated_at' => '2020-06-07 12:39:37',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Playlist seeded by Queens Of The Stone Age',
                'artist_id' => 99,
                'created_at' => '2020-06-07 12:41:58',
                'updated_at' => '2020-06-07 12:41:58',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Playlist seeded by Foo Fighters',
                'artist_id' => 101,
                'created_at' => '2020-06-07 12:45:18',
                'updated_at' => '2020-06-07 12:45:18',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Playlist seeded by Nick Cave',
                'artist_id' => 172,
                'created_at' => '2020-06-07 12:49:19',
                'updated_at' => '2020-06-07 12:49:19',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Playlist seeded by Godspeed You! Black Emperor',
                'artist_id' => 214,
                'created_at' => '2020-06-07 12:50:11',
                'updated_at' => '2020-06-07 12:50:11',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Playlist seeded by Faith No More',
                'artist_id' => 138,
                'created_at' => '2020-06-07 12:53:22',
                'updated_at' => '2020-06-07 12:53:22',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Playlist seeded by 10,000 Maniacs',
                'artist_id' => 50,
                'created_at' => '2020-06-07 21:25:18',
                'updated_at' => '2020-06-07 21:25:18',
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Playlist seeded by Gordi',
                'artist_id' => 1,
                'created_at' => '2020-06-07 21:26:57',
                'updated_at' => '2020-06-07 21:26:57',
            ),
            9 => 
            array (
                'id' => 10,
                'title' => 'Playlist seeded by Peter Gabriel',
                'artist_id' => 296,
                'created_at' => '2020-06-07 21:36:00',
                'updated_at' => '2020-06-07 21:36:00',
            ),
            10 => 
            array (
                'id' => 11,
                'title' => 'Playlist seeded by Hilltop Hoods',
                'artist_id' => 333,
                'created_at' => '2020-06-07 22:09:44',
                'updated_at' => '2020-06-07 22:09:44',
            ),
            11 => 
            array (
                'id' => 12,
                'title' => 'Playlist seeded by The Oh Hellos',
                'artist_id' => 382,
                'created_at' => '2020-06-08 23:56:58',
                'updated_at' => '2020-06-08 23:56:58',
            ),
            12 => 
            array (
                'id' => 13,
                'title' => 'Playlist seeded by Kodak Black',
                'artist_id' => 431,
                'created_at' => '2020-06-09 00:01:32',
                'updated_at' => '2020-06-09 00:01:32',
            ),
            13 => 
            array (
                'id' => 14,
                'title' => 'Playlist seeded by Chvrches',
                'artist_id' => 480,
                'created_at' => '2020-06-09 00:13:21',
                'updated_at' => '2020-06-09 00:13:21',
            ),
            14 => 
            array (
                'id' => 15,
                'title' => 'Playlist seeded by Pink Floyd',
                'artist_id' => 527,
                'created_at' => '2020-06-09 00:18:11',
                'updated_at' => '2020-06-09 00:18:11',
            ),
            15 => 
            array (
                'id' => 16,
                'title' => 'Playlist seeded by Carly Rae Jepsen',
                'artist_id' => 546,
                'created_at' => '2020-06-09 00:24:45',
                'updated_at' => '2020-06-09 00:24:45',
            ),
            16 => 
            array (
                'id' => 17,
                'title' => 'Playlist seeded by Father John Misty',
                'artist_id' => 593,
                'created_at' => '2020-06-09 01:23:53',
                'updated_at' => '2020-06-09 01:23:53',
            ),
            17 => 
            array (
                'id' => 18,
                'title' => 'Playlist seeded by Pink Floyd',
                'artist_id' => 527,
                'created_at' => '2020-06-09 01:41:24',
                'updated_at' => '2020-06-09 01:41:24',
            ),
            18 => 
            array (
                'id' => 19,
                'title' => 'Playlist seeded by Hooligan Hefs',
                'artist_id' => 640,
                'created_at' => '2020-06-09 01:53:50',
                'updated_at' => '2020-06-09 01:53:50',
            ),
            19 => 
            array (
                'id' => 20,
                'title' => 'Playlist seeded by Bob Dylan',
                'artist_id' => 192,
                'created_at' => '2020-06-09 22:05:56',
                'updated_at' => '2020-06-09 22:05:56',
            ),
            20 => 
            array (
                'id' => 21,
                'title' => 'Playlist seeded by Queen',
                'artist_id' => 163,
                'created_at' => '2020-06-09 22:33:59',
                'updated_at' => '2020-06-09 22:33:59',
            ),
            21 => 
            array (
                'id' => 22,
                'title' => 'Playlist seeded by Ruelle',
                'artist_id' => 710,
                'created_at' => '2020-06-09 22:58:28',
                'updated_at' => '2020-06-09 22:58:28',
            ),
            22 => 
            array (
                'id' => 23,
                'title' => 'Playlist seeded by Red Hot Chili Peppers',
                'artist_id' => 115,
                'created_at' => '2020-06-09 23:16:02',
                'updated_at' => '2020-06-09 23:16:02',
            ),
            23 => 
            array (
                'id' => 24,
                'title' => 'Playlist seeded by Van Morrison',
                'artist_id' => 79,
                'created_at' => '2020-06-09 23:19:40',
                'updated_at' => '2020-06-09 23:19:40',
            ),
            24 => 
            array (
                'id' => 25,
                'title' => 'Playlist seeded by Paul Kalkbrenner',
                'artist_id' => 788,
                'created_at' => '2020-06-10 03:54:17',
                'updated_at' => '2020-06-10 03:54:17',
            ),
            25 => 
            array (
                'id' => 26,
                'title' => 'Playlist seeded by Pink Floyd',
                'artist_id' => 527,
                'created_at' => '2020-06-10 06:32:54',
                'updated_at' => '2020-06-10 06:32:54',
            ),
            26 => 
            array (
                'id' => 27,
                'title' => 'Playlist seeded by Valerie Broussard',
                'artist_id' => 740,
                'created_at' => '2020-06-10 06:53:36',
                'updated_at' => '2020-06-10 06:53:36',
            ),
            27 => 
            array (
                'id' => 28,
                'title' => 'Playlist seeded by Blink 182',
                'artist_id' => 156,
                'created_at' => '2020-06-11 13:22:21',
                'updated_at' => '2020-06-11 13:22:21',
            ),
            28 => 
            array (
                'id' => 29,
                'title' => 'Playlist seeded by Midori Takada',
                'artist_id' => 910,
                'created_at' => '2020-06-12 09:49:32',
                'updated_at' => '2020-06-12 09:49:32',
            ),
            29 => 
            array (
                'id' => 30,
                'title' => 'Playlist seeded by The Flock',
                'artist_id' => 959,
                'created_at' => '2020-06-12 09:51:44',
                'updated_at' => '2020-06-12 09:51:44',
            ),
            30 => 
            array (
                'id' => 31,
                'title' => 'Playlist seeded by Susumu Hirasawa',
                'artist_id' => 979,
                'created_at' => '2020-06-12 09:52:57',
                'updated_at' => '2020-06-12 09:52:57',
            ),
            31 => 
            array (
                'id' => 32,
                'title' => 'Playlist seeded by Say Lou Lou',
                'artist_id' => 1027,
                'created_at' => '2020-06-12 09:57:37',
                'updated_at' => '2020-06-12 09:57:37',
            ),
            32 => 
            array (
                'id' => 33,
                'title' => 'Playlist seeded by Candy Claws',
                'artist_id' => 1074,
                'created_at' => '2020-06-12 09:58:49',
                'updated_at' => '2020-06-12 09:58:49',
            ),
            33 => 
            array (
                'id' => 34,
                'title' => 'Playlist seeded by Marcos Valle',
                'artist_id' => 1075,
                'created_at' => '2020-06-12 10:00:06',
                'updated_at' => '2020-06-12 10:00:06',
            ),
            34 => 
            array (
                'id' => 35,
                'title' => 'Playlist seeded by Eruca Sativa',
                'artist_id' => 1076,
                'created_at' => '2020-06-12 10:01:30',
                'updated_at' => '2020-06-12 10:01:30',
            ),
            35 => 
            array (
                'id' => 36,
                'title' => 'Playlist seeded by James Egbert',
                'artist_id' => 1224,
                'created_at' => '2020-06-12 10:14:22',
                'updated_at' => '2020-06-12 10:14:22',
            ),
            36 => 
            array (
                'id' => 37,
                'title' => 'Playlist seeded by Matt Walden',
                'artist_id' => 1273,
                'created_at' => '2020-06-12 10:15:24',
                'updated_at' => '2020-06-12 10:15:24',
            ),
            37 => 
            array (
                'id' => 38,
                'title' => 'Playlist seeded by Matt Walden',
                'artist_id' => 1273,
                'created_at' => '2020-06-12 10:15:24',
                'updated_at' => '2020-06-12 10:15:24',
            ),
            38 => 
            array (
                'id' => 39,
                'title' => 'Playlist seeded by Thrice',
                'artist_id' => 1298,
                'created_at' => '2020-06-12 11:18:43',
                'updated_at' => '2020-06-12 11:18:43',
            ),
            39 => 
            array (
                'id' => 40,
                'title' => 'Playlist seeded by Viikate',
                'artist_id' => 1333,
                'created_at' => '2020-06-12 13:47:54',
                'updated_at' => '2020-06-12 13:47:54',
            ),
            40 => 
            array (
                'id' => 41,
                'title' => 'Playlist seeded by Moonsorrow',
                'artist_id' => 1381,
                'created_at' => '2020-06-12 14:22:20',
                'updated_at' => '2020-06-12 14:22:20',
            ),
            41 => 
            array (
                'id' => 42,
                'title' => 'Playlist seeded by John Legend',
                'artist_id' => 1430,
                'created_at' => '2020-06-14 20:41:46',
                'updated_at' => '2020-06-14 20:41:46',
            ),
            42 => 
            array (
                'id' => 43,
                'title' => 'Playlist seeded by Eminem',
                'artist_id' => 374,
                'created_at' => '2020-06-14 21:13:27',
                'updated_at' => '2020-06-14 21:13:27',
            ),
        ));
        
        
    }
}