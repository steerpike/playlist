<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(ArtistsTableSeeder::class);
        $this->call(TracksTableSeeder::class);
        $this->call(PlaylistsTableSeeder::class);
        $this->call(PlaylistsTracksTableSeeder::class);
        $this->call(RelationshipsTableSeeder::class);
    }
}
