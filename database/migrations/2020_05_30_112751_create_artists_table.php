<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist')->nullable();
            $table->string('musicmap_name')->nullable();
            $table->string('lastfm_name')->nullable();
            $table->string('musicmap_url')->nullable();
            $table->string('lastfm_url')->nullable();
            $table->boolean('crawled')->default(false);
            $table->boolean('seed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
