<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('artists', 'ArtistController');
Route::resource('artists.tracks', 'TrackController')->shallow();
Route::get('/playlists', 'PlaylistController@index');
Route::get('/create/{id}', 'PlaylistController@create');
Route::get('/playlists/{id}/{token?}', 'PlaylistController@show');

Route::get('/auth', 'ArtistController@test');
Route::get('/', function () {
    return view('welcome');
});
