<div>
    <input wire:model="search" type="text" placeholder="Search playlists..."/>
    <ul>
        @foreach($playlists as $playlist)
            <li><a href="/playlists/{{ $playlist->id }}">{{ $playlist->title }}</a> created {{ \Carbon\Carbon::parse($playlist->created_at)->diffForhumans() }}</li>
        @endforeach
        {{$playlists->links()}} 
    </ul>
</div>
