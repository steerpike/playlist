<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Playlist Generator</title>

        <!-- Fonts -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/mvp.css" rel="stylesheet">

       
    </head>
    <body>
        <main>
            <h1>Playlist Generator</h1>
            @if (session('error'))
                <div>
                    <mark>{{ session('error') }}</mark>
                </div>
            @endif
            @if (session('info'))
                <div>
                    {{ session('info') }}
                    <p>In a few minutes it should be available at the <a href='/playlists'>playlist listing page</a></p>
                </div>
            @endif
            <form method="POST" action="/artists">
                @csrf
                <label for="musicmap_url">Artist name</label>
                <input type="text" name="musicmap_url" placeholder="e.g. pink floyd" />
                @error('musicmap_url')
                <mark>{{$errors->first('musicmap_url')}}</mark>
                @enderror
                <input type="submit" value="Create" />
            </form>
        </main>
    </body>
</html>
