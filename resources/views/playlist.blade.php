<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Playlist Generator</title>

        <!-- Fonts -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/mvp.css" rel="stylesheet">
    </head>
    <body>
        <main>
        <h1>{{$playlist->title}}</h1>

        <section>
            @php
            $video_list = '';
            $first = '';
            @endphp
            @foreach ($playlist->tracks as $track)
                @php
                $strArray = explode('=',$track->youtube_url);
                $youtube_id = end($strArray);
                if($first == '' ) {
                    $first = $youtube_id;
                } else {
                    $video_list .= $youtube_id.",";
                }
                
                @endphp
            @endforeach
            @php
            $video_list = substr($video_list, 0, -1);    
            @endphp
            <iframe title={{$playlist->title}}  id="ytplayer" type="text/html" width="720" height="405" src="https://www.youtube.com/embed/{{$first}}?playlist={{$video_list}}&playsinline=1" frameborder="0" allowfullscreen>
        </section>
        </main>
    </body>

</html>
