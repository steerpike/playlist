<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Playlist Generator</title>

        <!-- Fonts -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/mvp.css" rel="stylesheet">
    </head>
    <body>
        <main>
            <h1>Playlists</h1>
            <p>If an artist you recently submitted doesn't appear, please give it a few minutes to
                before refreshing. It takes a while to collect all the related artists and tracks.
            </p>
            @if (session('error'))
                <div>
                    <mark>{{ session('error') }}</mark>
                </div>
            @endif
            @if (session('info'))
                <div>
                    {{ session('info') }}
                </div>
            @endif
            @livewire('search')
        </main>
        @livewireScripts
    </body>
</html>
