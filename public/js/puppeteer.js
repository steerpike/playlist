const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
    });
    const page = await browser.newPage();

    await page.goto('https://accounts.google.com/signin/v2/identifier?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3Dhttps%253A%252F%252Fwww.youtube.com%252F&hl=en&ec=65620&flowName=GlifWebSignIn&flowEntry=ServiceLogin', { "waitUntil": "networkidle0" });
    await page.waitFor(2000);

    await page.waitForSelector('input[type="email"]')
    await page.type('input[type="email"]', 'drop.table.people@gmail.com')
    await page.click('#identifierNext')

    await page.waitForSelector('input[type="password"]', { visible: true })
    await page.type('input[type="password"]', 'twm@ts4twhpa')

    await page.waitForSelector('#passwordNext', { visible: true })
    await page.click('#passwordNext')


    await page.screenshot({ path: 'example.png' });

    await browser.close();
})();