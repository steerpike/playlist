<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    protected $guarded = [];
    public function artist()
    {
        return $this->belongsTo('App\Artist');
    }
    public function playlists()
    {
      return $this->belongsToMany('App\Playlist','playlists_tracks','playlist_id','track_id');
    }
}
