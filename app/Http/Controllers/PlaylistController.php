<?php

namespace App\Http\Controllers;

use App\Playlist;
use App\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class PlaylistController extends Controller
{
    public function index()
    {
        $playlists = Playlist::orderBy('created_at', 'DESC')->paginate(25);
        /*
        foreach($playlists as $playlist) {
            echo $playlist->title." (".$playlist->id.")<br />";
        }*/
        return view('playlists', ['playlists'=>$playlists]);
    }
    public function show(Request $request, $id, $token=null)
    {
        $playlist = Playlist::with('tracks')->findOrFail($id);
        return view('playlist', ['playlist'=>$playlist]);
    }
    public function create($id)
    {
        $artist = Artist::findOrFail($id);
        $tracks = array();
        foreach($artist->tracks as $track) {
            echo $track->id." ".$track->name."<br />";
            $tracks[] = $track->id;
        }
        foreach($artist->related as $related) {
            foreach($related->tracks as $rtrack) {
                //Log::info("Adding track ".$rtrack->name);
                echo $rtrack->id." ".$rtrack->name."<br />";
                $tracks[] = $rtrack->id;
            }
        }
        //shuffle($tracks);
        $listing = array_rand($tracks, 50);
        $result = array();
        for($x=0; $x<=sizeof($listing)-1; $x++)
        {
            $result[] = $tracks[$listing[$x]];
        }

        dd($result);
        $playlist = new Playlist;
        $playlist->title = "Playlist seeded by ".$artist->artist;
        $playlist->artist_id = $artist->id;
        //$playlist->save();
        //$playlist->tracks()->attach($listing);
    }
}
