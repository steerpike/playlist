<?php

namespace App\Http\Controllers;

use PHPHtmlParser\Dom;
use App\Artist;
use App\Playlist;
use App\Jobs\CreateArtist;
use App\Jobs\CreatePlaylist;
use App\Jobs\GetRelatedArtists;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artists = Artist::all();
        foreach($artists as $artist) {
            echo $artist->musicmap_url."<br />";
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('welcome');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'musicmap_url' => 'required'
        ]);
        $contains = Str::contains($request['musicmap_url'], 'https://www.music-map.com/');
        Log::info("Request made from ".$request->ip()." for ".$request['musicmap_url']);
        if($contains) {
            $url = $request['musicmap_url'];
        } else {
            $oname = $request['musicmap_url'];
            $name = str_replace(" ", "+", $oname);
            $url = "https://www.music-map.com/".$name;
        }
        try {
            $artist = Artist::with('related')->where('musicmap_url', '=', $url)->firstOrFail();
            $name = $artist->artist;
            if($artist->related()->count()===0){
                GetRelatedArtists::withChain([
                new CreatePlaylist($artist)
              ])->dispatch($artist->musicmap_url);
            } else {
                CreatePlaylist::dispatch($artist);
            }
        } catch(ModelNotFoundException $e) {
            $dom = new Dom;
            $dom->loadFromUrl($url);
            $html = $dom->outerHtml;
            $name = $dom->find("#the_title")->text();
            if($name === "I got 404 problems") {
                Log::error("Received a 404 attempting to get info for: ".$url);
                return redirect('/')->with('error', 'No artist found for '.$url);
            } else {
                CreateArtist::dispatch($dom, true);
            }
            
        }
        return redirect('/')->with('info', 'Creating a playlist for '.$name.'.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    {
        echo $artist->artist."<br />";
        echo "<h4>Tracks</h4>";
        foreach($artist->tracks as $track) {
            echo $track->name." | ".$track->youtube_url."<br />";
        }
        echo "<h4>Related</h4>";
        foreach($artist->related as $related) {
            echo $related->artist."<br />";
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        //
    }

    public function test(Request $request) 
    {
        $client = new \Google_Client();
        $client->setApplicationName('Playlist Generator');
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube.force-ssl',
        ]);
        $data = '{"web":{"client_id":"475719231326-tosam9siam22g1t4jk4u52568icvjku2.apps.googleusercontent.com","project_id":"playlistgenerator-279205","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"VWWS3jVNdsyZCAL-fIcmaKOF","redirect_uris":["http://localhost:8000/auth"],"javascript_origins":["http://localhost:8000"]}}';
        $client->setAuthConfig(json_decode($data, true));
        $client->setAccessType('offline');

        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open this link in your browser:\n%s\n", $authUrl);
        echo "<a href='".$authUrl."'>Click this link</a>";
        print('Enter verification code: ');
        $authCode = $request['code'];
        print_r($authCode);
        if($authCode) {
            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Define service object for making API requests.
            $service = new \Google_Service_YouTube($client);

            // Define the $playlist object, which will be uploaded as the request body.
            $playlist = new \Google_Service_YouTube_Playlist();

            // Add 'snippet' object to the $playlist object.
            $playlistSnippet = new \Google_Service_YouTube_PlaylistSnippet();
            $playlistSnippet->setTitle('Playlist seeded by Gordi');
            $playlist->setSnippet($playlistSnippet);

            $response = $service->playlists->insert('snippet', $playlist);
            echo "ID:<br />";
            echo $response['id'];
            // Define the $playlistItem object, which will be uploaded as the request body.
            $playlistItem = new \Google_Service_YouTube_PlaylistItem();

            // Add 'snippet' object to the $playlistItem object.
            $playlistItemSnippet = new \Google_Service_YouTube_PlaylistItemSnippet();
            $playlistItemSnippet->setPlaylistId($response['id']);
            $playlist = Playlist::findOrFail(1);
            foreach($playlist->tracks as $track){
                try {
                $strArray = explode('=',$track->youtube_url);
                $youtube_id = end($strArray);
                $resourceId = new \Google_Service_YouTube_ResourceId();
                $resourceId->setKind('youtube#video');
                $resourceId->setVideoId($youtube_id);
                $playlistItemSnippet->setResourceId($resourceId);
                $playlistItem->setSnippet($playlistItemSnippet);
                $response = $service->playlistItems->insert('snippet', $playlistItem);
                } catch(\Exception $e) {
                    Log::error("Error adding  (".$track->id.")".$track->name.
                    " for File:".$e->getFile()." ".
                    "Line: ".$e->getLine()." ".
                    "Error: ".$e->getMessage()." ENDS");
                }
            }
        }
    }
}
