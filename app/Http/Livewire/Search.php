<?php

namespace App\Http\Livewire;

use App\Playlist;
use Livewire\Component;
use Livewire\WithPagination;

class Search extends Component
{
    use WithPagination;
    public $search = '';
    //public $playlists;
    public $currentPage = 1;
    public function setPage($url)
    {
        dd($url);
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function() {
            return $this->currentPage;
        });
    }
    public function render()
    {
        //$this->playlists = Playlist::paginate(15);
        $search = "%".$this->search."%";
        if($this->search == '') {
            $playlists = Playlist::orderBy('created_at', 'DESC')->paginate(10);
        } else {
            $playlists = Playlist::where('title', 'like', $search)->paginate(10);
        }
        return view('livewire.search', [
            'playlists' => $playlists,
        ]);
    }
}
