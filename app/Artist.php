<?php

namespace App;

use App\Jobs\GetTracks;
use App\Jobs\GetRelatedArtists;
use App\Jobs\CreatePlaylist;
use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    //
    protected $guarded = [];
    public function related()
    {
      return $this->belongsToMany('App\Artist','relationships','seed_id','related_id');
    }
    public function seeds()
    {
      return $this->belongsToMany('App\Artist','relationships','related_id','seed_id');
    }
    public function tracks()
    {
        return $this->hasMany('App\Track');
    }
    protected static function booted()
    {
        static::created(function ($artist) {
            GetTracks::dispatch($artist);
            if($artist->seed === true) {
              GetRelatedArtists::withChain([
                new CreatePlaylist($artist)
              ])->dispatch($artist->musicmap_url);
            }
        });
    }
}
