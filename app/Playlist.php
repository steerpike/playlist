<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $guarded = [];
    public function artist()
    {
        return $this->belongsTo('App\Artist');
    }
    public function tracks()
    {
      return $this->belongsToMany('App\Track','playlists_tracks','playlist_id','track_id');
    }
}
