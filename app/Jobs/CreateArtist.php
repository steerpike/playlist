<?php

namespace App\Jobs;

use App\Artist;
use PHPHtmlParser\Dom;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateArtist implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $dom;
    protected $artist;
    protected $seed;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dom, $seed=false)
    {
        $this->dom = $dom;
        $this->seed = $seed;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            //$dom = new Dom;
            //$dom->loadFromUrl($this->url);
            //$html = $dom->outerHtml;
            $name = $this->dom->find("#the_title")->text();
            if($name === "I got 404 problems") {
                Log::error("Received a 404 attempting to get info for: ".$this->url);
            } else {
                $artist = new Artist;
                $artist->artist = $name;
                $link = $this->dom->find("#s0")->getAttribute('href');
                $basename = Str::of($link)->basename(".html");
                $artist->musicmap_name = $basename;
                $lastfm_name = str_replace(" ", "+", $name);
                $artist->lastfm_name = $lastfm_name;
                $artist->musicmap_url = "https://www.music-map.com/".$basename;
                $artist->lastfm_url = "https://last.fm/music/".$lastfm_name;
                $artist->seed = $this->seed;
                $artist->save();
                $this->artist = $artist;
            }
        } catch (\Exception $e) {
            Log::error("Encountered an error processing Artist ".
            "at url: ".
            "for File:".$e->getFile()." ".
            "Line: ".$e->getLine()." ".
            "Error: ".$e->getMessage()." ENDS");
        }
        
    }
}
