<?php

namespace App\Jobs;

use App\Artist;
use App\Playlist;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreatePlaylist implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $artist;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::info("Calling Playlist Generation - seeded by ".$this->artist->artist);
            $tracks = array();
            foreach($this->artist->tracks as $track) {
                $tracks[] = $track->id;
            }
            foreach($this->artist->related as $related) {
                foreach($related->tracks as $rtrack) {
                    $tracks[] = $rtrack->id;
                }
            }
            //shuffle($tracks);
            $listing = array_rand($tracks, 50);
            $result = array();
            for($x=0; $x<=sizeof($listing)-1; $x++)
            {
                $result[] = $tracks[$listing[$x]];
            }
            $playlist = new Playlist;
            $playlist->title = "Playlist seeded by ".$this->artist->artist;
            $playlist->artist_id = $this->artist->id;
            $playlist->save();
            $playlist->tracks()->attach($result);
        } catch(\Exception $e) {
            Log::error("Error generating playlist for ".$this->artist->artist.
            " for File:".$e->getFile()." ".
            "Line: ".$e->getLine()." ".
            "Error: ".$e->getMessage()." ENDS");
        }
    }
}
