<?php

namespace App\Jobs;

use App\Artist;
use PHPHtmlParser\Dom;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GetRelatedArtists implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $url;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $url)
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $artist = Artist::where('musicmap_url', '=', $this->url)->firstOrFail();
            $dom = new Dom;
            $dom->loadFromUrl($this->url);
            $related_artists_links = $dom->find("#gnodMap > a");
            $ids = array();
            foreach($related_artists_links as $related_artists_link) {
                $html_id = $related_artists_link->getAttribute('id');
                if($html_id === "s0") { continue; }
                $related_artist_href = $related_artists_link->getAttribute('href');
                $related_artist_musicmap_url = "https://www.music-map.com/".$related_artist_href;
                $related_artist_name = $related_artists_link->text();
                $related_artist_lastfm_name = str_replace(" ", "+", $related_artist_name);
                $related_artist_lastfm_url = "https://last.fm/music/".$related_artist_lastfm_name;
                $related_artist = Artist::updateOrCreate(['musicmap_url'=>$related_artist_musicmap_url],
                ['artist' => $related_artist_name,
                'musicmap_name' => $related_artist_href,
                'lastfm_name' => $related_artist_lastfm_name,
                'lastfm_url' => $related_artist_lastfm_url]
                );
                //Log::info("Created ", [$related_artist_name]);
                $ids[] = $related_artist->id;
            }
            $artist->related()->sync($ids);
        } catch(ModelNotFoundException $e) {
            Log::error("Failed trying to find artist for ".$this->url." when ".
            "attempting to get related artists");
        } catch( \Exception $e) {
            Log::error("Encountered an error getting Related Artist ".
            "at url: ".$this->url." ".
            "for File:".$e->getFile()." ".
            "Line: ".$e->getLine()." ".
            "Error: ".$e->getMessage()." ENDS");
        }
    }
}
