<?php

namespace App\Jobs;

use App\Track;
use App\Artist;
use PHPHtmlParser\Dom;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetTracks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $artist;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            //echo "ARTIST: ".$this->artist->artist." COUNT:".count($this->artist->tracks);
            if(count($this->artist->tracks) === 0) {
                $dom = new Dom;
                $track_url = $this->artist->lastfm_url."/+tracks";
                $dom->loadFromUrl($track_url);
                $trackList = $dom->find('.chartlist-play-button');
                if(count($trackList) === 0){
                    Log::info("Found no tracks for ".$track_url.".");
                }
                foreach($trackList as $item) 
                {
                    //$link = $item->find('a');
                    //$item->{'data-track-url'} = $item->href;
                    $name = $item->{'data-track-name'};
                    $url = $item->href;
                    $track = Track::updateOrCreate(
                        ['youtube_url' => $url],
                        ['name' => $name,
                        'youtube_url' => $url,
                        'artist_id'=>$this->artist->id]
                    );
                    $track->save();
                }
            } else {
                Log::info("Already gathered tracks for ".$this->artist->artist.". Skipping.");
            }

            
        } catch(\Exception $e) {
            Log::error("Error getting tracks for ".$this->artist->artist.
            " for File:".$e->getFile()." ".
            "Line: ".$e->getLine()." ".
            "Error: ".$e->getMessage()." ENDS");
        }
        
    }
}
